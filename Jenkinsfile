#!/usr/bin/env groovy

/*
  Copyright (C) 2017 Collabora Limited
  Author: Guillaume Tucker <guillaume.tucker@collabora.com>

  This module is free software; you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation; either version 2.1 of the License, or (at your option)
  any later version.

  This library is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
  details.

  You should have received a copy of the GNU Lesser General Public License
  along with this library; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/* Note: docker_image.inside() needs to be called after dir(some_directory)
 * otherwise it fails and stays at the root of the workspace. */

def BUILDROOT_URL = "https://gitlab.collabora.com/gtucker/buildroot.git"
def BUILDROOT_BRANCH = "wip-qemu-x86"
/*def BUILDROOT_BRANCH = "2016.05-health-checks"*/
def REGISTRY = "docker-registry.internal.collabora.com"

node("docker-slave") {
    docker.withRegistry("https://${REGISTRY}",
                        "ccu-docker-internal-collabora-com") {
        def docker_image = null
        def out_dir_top = env.WORKSPACE + '/output/buildroot'
        def out_dir = out_dir_top + "/${currentBuild.number}"

        /* debug */
        dir("checkout") {
            deleteDir()
        }

        stage("job checkout") {
            dir("checkout") {
                git(url: env.gitlabSourceRepoHttpUrl,
                    branch: env.gitlabBranch,
                    poll: false)
            }
        }

        stage("builder image") {
            dir("checkout") {
                docker_image = docker.build(
                    "${REGISTRY}/reference-build/buildroot",
                    "docker")
                docker_image.push()
            }
        }

        stage("init") {
            dir(out_dir_top) {
                deleteDir()
            }

            /*sh(script: "rm -rf ${out_dir_top}")*/

            dir(out_dir) {
                def params = """\
url:        ${BUILDROOT_URL}
branch:     ${BUILDROOT_BRANCH}
"""
                sh(script: 'echo "' + "${params}" + '" > manifest.txt')
            }
        }

        node("docker-slave") {
            def workdir = env.WORKSPACE + '/buildroot'

            stage("buildroot checkout") {
                dir(workdir) {
                    docker_image.inside() {
                        git(url: BUILDROOT_URL,
                            branch: BUILDROOT_BRANCH,
                            poll: false)
                    }
                }
            }

            stage("buildroot run (native)") {
                def arch = sh(returnStdout: true, script: "uname -m").trim()
                def build_dir = env.WORKSPACE + "/build-native"
                sh(script: "mkdir -p ${build_dir}")
                docker_image.inside() {
                    sh(script: """
cd ${workdir}
#make O=${build_dir} clean
#make O=${build_dir} distclean
#make O=${build_dir} qemu_x86_64_defconfig
#make O=${build_dir} -j1
#ls -l ${build_dir}/images || echo never mind

make clean
make distclean
make qemu_x86_64_defconfig
make -j1
ls -l output/images || echo never mind

#cp ${build_dir}/output/images/rootfs.cpio.gz ${out_dir}/buildroot-${arch}.cpio.gz
""")
                }
            }

            stage("buildroot run (arm64)") {
/*
                def arch = "arm64"
                def build_dir = env.WORKSPACE + "/build-${arch}"
                echo "build_dir: ${build_dir}"
                sh(script: "mkdir -p ${build_dir}")
                docker_image.inside("--device=/dev/kvm") {
                    sh(script: """
cd ${workdir}
make O=${build_dir} clean
make O=${build_dir} distclean
make O=${build_dir} qemu_aarch64_virt_defconfig
make O=${build_dir} -j1
ls -l ${build_dir}/images || echo never mind
#mkdir -p ${build_dir}/output/images
#echo DUMMY >  ${build_dir}/output/images/rootfs.cpio.gz
#cp ${build_dir}/output/images/rootfs.cpio.gz ${out_dir}/buildroot-${arch}.cpio.gz
""")
                }
*/
            }

            stage("buildroot run (arm)") {
/*
                def arch = "arm"
                def build_dir = env.WORKSPACE + "/build-${arch}"
                sh(script: "mkdir -p ${build_dir}")
                docker_image.inside("--device=/dev/kvm") {
                    sh(script: """
cd ${workdir}
make O=${build_dir} clean
make O=${build_dir} distclean
make O=${build_dir} qemu_arm_vexpress_defconfig
make O=${build_dir} -j8
ls -l ${build_dir}/output/images
#mkdir -p ${build_dir}/output/images
#echo DUMMY >  ${build_dir}/output/images/rootfs.cpio.gz
#cp ${build_dir}/output/images/rootfs.cpio.gz ${out_dir}/buildroot-${arch}.cpio.gz
""")
                }
*/
            }
        }

        stage("upload") {
/*
            dir(out_dir_top) {
                docker_image.inside("--device=/dev/kvm") {
                    env.NSS_WRAPPER_PASSWD = '/tmp/passwd'
                    env.NSS_WRAPPER_GROUP = '/dev/null'
                    sshagent (credentials: ["images.ccu-upload",]) {
                        sh(script: 'echo docker:x:$(id -u):$(id -g):docker gecos:/tmp:/bin/false > ${NSS_WRAPPER_PASSWD}')
                        sh(script: 'LD_PRELOAD=libnss_wrapper.so ssh -oStrictHostKeyChecking=no jenkins-upload@images.collabora.co.uk "mkdir -p /srv/images.collabora.co.uk/www/images/singularity/reference/buildroot"')
                        sh(script: 'LD_PRELOAD=libnss_wrapper.so rsync -e "ssh -oStrictHostKeyChecking=no" -av --dry-run ' + "${currentBuild.number}" + ' jenkins-upload@images.collabora.co.uk:/srv/images.collabora.co.uk/www/images/singularity/reference/buildroot/')
                        sh(script: "rm -rf ${currentBuild.number}")
                    }
                }
            }
*/
        }
    }
}
